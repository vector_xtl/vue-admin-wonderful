import request from '@/utils/request'



//updateAndSaveban 添加或者修改班级信息
export  function  updateAndSaveban(param){
    return request({
        url:'/banji/updateAndSaveban',
        method:'post',
        data:param
    })
}
export  function  getBanJiById(param){
    return request({
        url:'/banji/getBanJiById/'+param,
        method:'get'
    })
}