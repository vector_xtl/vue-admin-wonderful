import request from '@/utils/request'



export  function  getPageByParam(param){
    return request({
        url:'/posts/search',
        method:'post',
        data:param
    })
}
export  function  getPosts(param){
    return request({
        url:'/posts/getPostsById/'+param,
        method:'get'
    })
}
//
export  function  savePosts(param){
    return request({
        url:'/posts/saveAndUpdate',
        method:'post',
        data:param
    })
}
export  function  deletPostsById(param){
    return request({
        url:'/posts/deletPostsById/'+param,
        method:'get',
    })
}