import request from '@/utils/request'

export  function  saveLeaves(param){
    return request({
        url:'/leave/saveLeaves',
        method:'post',
        data:param
    })
}
export  function  getAllLeavesByParam(param){
    return request({
        url:'/leave/getAllLeaves',
        method:'post',
        data:param
    })
}
export  function  getLeaves(param){
    return request({
        url:'/leave/getLeavesById/'+param,
    })
}