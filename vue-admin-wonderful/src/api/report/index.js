import request from '@/utils/request'



export  function  reportsSearch(param){
    return request({
        url:'/report/search',
        method:'post',
        data:param
    })
}
export  function  notReportUser(param){
    return request({
        url:'/report/getNotReportUser',
        method:'post',
        data:param
    })
}