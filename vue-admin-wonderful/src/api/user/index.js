import request from '@/utils/request'




//getAllClassByFid 通过辅导员id查询所管辖的用户
export  function  getAllClassByFid(value){
    return request({
        url:'/banji/getAllBanji/'+value,
        method:'get',
    })
}
//getPageByParam 条件分页查询
export function  getPageByParam(param){
    return request({
        url:'/user/getPageByParam',
        method:'post',
        data:param
    })
}
// 修改用户
export function  updateUser(param){
    return request({
        url:'/user/updateUser',
        method:'post',
        data:param
    })
}
export function  getUserById(param){
    return request({
        url:'/user/getUserById/'+param,
        method:'get',
    })
}
export function  deletUserById(param){
    return request({
        url:'/user/deletUserById/'+param,
        method:'get',
    })
}