import request from '@/utils/request'


export  function  saveQmsg(param){
    return request({
        url:'/message/save',
        method:'post',
        data:param
    })
}

export  function  getPageByParam(param){
    return request({
        url:'/message/search',
        method:'post',
        data:param
    })
}
export  function  getqmsg(param){
    return request({
        url:'/message/getQmsgById/'+param,
    })
}