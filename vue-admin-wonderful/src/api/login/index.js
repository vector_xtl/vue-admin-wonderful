import request from '@/utils/request'

// 用户登录
export function signIn(params) {
    return request({
        url: '/finstructor/login',
        method: 'post',
        data: params
    })
}

export function saveOrUpdate(params) {
    return request({
        url: '/finstructor/save',
        method: 'post',
        data: params
    })
}
// 用户退出登录
export function signOut() {
    return request({
        url: '/user/signOut',
        method: 'post',
        data: params
    })
}